import subprocess

res = subprocess.run(["nvidia-smi"], capture_output=True)
gpu_info = res.stdout.decode("utf-8").split('Processes')[1].split('\n')
active_processes = []

for line in gpu_info[4:-2]:
    if line.split()[1] == 'No':
       print('No gpus active')
       break
    pid = line.split()[4]
    active_processes.append(pid)

for pid in active_processes:
    print('pid:', pid)
    subprocess.run(["kill", pid])
