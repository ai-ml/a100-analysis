import subprocess
import time
import os
import boto3

print('Landed on the node:' + os.getenv('NODE_NAME'))
print('Monitoring the job:' + os.getenv('JOB_NAME'))
print('Sleeping while the job starts')

interval = 5
multiplier = 30
active_gpus = set()
filename = os.getenv('JOB_NAME') + '-' + os.getenv('NODE_NAME')

with open(filename, 'w') as f:
    f.write('job_name:' + os.getenv('JOB_NAME') + '\n')
    f.write('node_name:' + os.getenv('NODE_NAME') + '\n')
    
client = boto3.client('s3', endpoint_url='https://s3.cern.ch')

try:
    client.download_file('dejan', filename, filename + '-local')
    while True:
        time.sleep(10000)
except:
    pass

for _ in range(multiplier):
    time.sleep(interval)

    res = subprocess.run(["nvidia-smi"], capture_output=True)
    gpu_info = res.stdout.decode("utf-8").split('Processes')[1].split('\n')

    for line in gpu_info[4:-2]:
        if line.split()[1] == 'No':
            print('No gpus active')
            break
        gpu_id = line.split()[1]
        active_gpus.add(gpu_id)

    print('active gpus so far:' + ' '.join(sorted(list(active_gpus))))

    with open(filename, 'a') as f:
        f.write('timestamp:' + str(time.time()) + '\n')
        f.write('active_gpus:' + ' '.join(sorted(list(active_gpus))) + '\n')
    
print('Sending the following content to the bucket:')
with open(filename, 'r') as f:
    for line in f.readlines():
        print(line)

client.upload_file(filename, 'dejan', filename)

print('Finished')
