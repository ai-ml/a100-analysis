Add credentials and create a secret   
```
kubectl apply -f s3-secret.yaml
```

Run training and monitoring
```
python3 submit.py
```