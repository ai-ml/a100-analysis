import time
import yaml
import uuid
import string
from kubernetes import client, config, utils
import random

for it in range(10):
    print('Running job ' + str(it))
    
    config.load_incluster_config()
    k8s_client = client.ApiClient()
    api = client.AppsV1Api(k8s_client)
    core_api = client.CoreV1Api(k8s_client)
    k8s_co_client = client.CustomObjectsApi()

    n_workers = 16
    namespace = 'kubeflow-user-example-com'
    random_string = ''.join(random.choices(string.ascii_lowercase + string.digits, k=10))
    job_name = 'n7-desy-3dgan-' + str(n_workers) + '-workers-' + random_string
    gpu_type = 'nvidia.com/gpu'

    pod_file = '/home/jovyan/a100-analysis/pod_clear_gpu.yaml'
    daemonset_file = '/home/jovyan/a100-analysis/daemonset.yaml'
    pytorchjob_file = '/home/jovyan/a100-analysis/pytorchjob.yaml'

    with open(pod_file) as f:
        pod_dict = yaml.safe_load(f)
        for i in range(9):
            pod_dict['metadata']['name'] = pod_dict['metadata']['name'][:17] + str(i)
            pod_dict['spec']['nodeName'] = pod_dict['spec']['nodeName'][:42] + str(i)
            pod_name = pod_dict['metadata']['name']

            print('Clearing GPU memory of the node ' + pod_dict['spec']['nodeName'])
            core_api.create_namespaced_pod(body=pod_dict, namespace=namespace)
            time.sleep(5)
            pod = core_api.read_namespaced_pod(name=pod_name, namespace=namespace)
            while pod.status.phase != 'Succeeded':
                print(pod.status.phase)
                pod = core_api.read_namespaced_pod(name=pod_name, namespace=namespace)
                time.sleep(1)
            print('Finished clearing GPU memory of the node ' + pod_dict['spec']['nodeName'])
            print('Deleting the pod')
            core_api.delete_namespaced_pod(name=pod_name, namespace=namespace)

    with open(daemonset_file) as f:
        daemonset_dict = yaml.safe_load(f)
        daemonset_name = daemonset_dict['metadata']['name']
        daemonset_dict['spec']['template']['spec']['containers'][0]['env'][2]['value'] = job_name
        print(daemonset_dict)

    print('Submitting daemonset ' + daemonset_name)
    api.create_namespaced_daemon_set(body=daemonset_dict, namespace=namespace)

    with open(pytorchjob_file, 'r') as f:
        pytorchjob_dict = yaml.safe_load(f)
        pytorchjob_dict['metadata']['name'] = job_name
        pytorchjob_dict['spec']['pytorchReplicaSpecs']['Worker']['replicas'] = n_workers - 1
        pytorchjob_dict['spec']['pytorchReplicaSpecs']['Master']['template']['spec']['containers'][0]['env'][1]['value'] = job_name
        print(pytorchjob_dict)

    print('Submitting pytorchjob ' + job_name)
    k8s_co_client.create_namespaced_custom_object(
        group='kubeflow.org',
        version='v1',
        namespace=namespace,
        plural='pytorchjobs',
        body=pytorchjob_dict,
    )

    interval = 5
    multiplier = 500

    for _ in range(multiplier):
        time.sleep(interval)

        resource = k8s_co_client.get_namespaced_custom_object(
            group='kubeflow.org',
            version='v1',
            namespace=namespace,
            plural='pytorchjobs',
            name=job_name,
        )

        if not 'status' in resource:
            continue

        status = resource['status']
        print(status)

        if 'completionTime' in status.keys():
            if status['completionTime']:
                break

    print('Deleting pytorchjob ' + job_name)
    k8s_co_client.delete_namespaced_custom_object(
        group='kubeflow.org',
        version='v1',
        namespace=namespace,
        plural='pytorchjobs',
        name=job_name,
    )

    print('Deleting daemonset ' + daemonset_name)
    resp = api.delete_namespaced_daemon_set(daemonset_dict['metadata']['name'], namespace)
    print(resp)
